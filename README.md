# Computed field
## Description
This module provide two extra formatters for the core daterange field.

 * **Dategroup formatter** to group start and end dates.
 * **Datediff formatter** to display the difference between start and end date.

## Usage

* Download and install the module as usual. No additional configuration is needed.
* Go to the tab "Manage display" then you can select and configure the formatter for any daterange field you wish to use.
**Don't forget to hit the *Save* button to store your changes** otherwise they are lost.

## Additional modules
There are no additional modules.

## Credits
"sdstyles": https://www.drupal.org/u/sdstyles for the DateGroupFormatter and the general idea


# Datetime Range Extra formatters
## Description
Provide extra datetime range formatters
This module provide two extra formatters for the core daterange field.
* **Dategroup formatter** to group start and end dates. Works very well with a date format 'F d, Y'.
* **Datediff formatter** to display the difference between start and end date. Used a 'Duration', add immediately 1 day. So if the start and end date are the same, duration is 1 day.

## Usage

* Download and install the module as usual. No additional configuration is needed.
* Go to the tab "Manage display" then you can select and configure the formatter for any daterange field you wish to use.
**Don't forget to hit the *Save* button to store your changes** otherwise they are lost.

## Additional modules
There are no additional modules.

##Credits
<a href="https://www.drupal.org/u/sdstyles">sdstyles</a>" for the <a href="https://www.drupal.org/project/date_group">Date Group formatter</a> r and the general idea for this module.
